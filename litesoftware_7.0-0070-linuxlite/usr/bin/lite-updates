#!/bin/bash
#---------------------------------------------------------------------------------------------------------
# Name: Linux Lite Updates
# Description: A GUI tool to easily install Updates in Linux Lite.
# Authors: Misko_2083, Jerry Bezencon, gerito1, Ralphy, Huzaifa Qureshi

# Website: https://www.linuxliteos.com
#---------------------------------------------------------------------------------------------------------

_APPNAME="Linux Lite Updates"                # Application name variable
_MYUPDUSER=$(whoami)
_ICON="/usr/share/icons/Papirus/24x24/apps/liteupdates.png"     # Lite Updates icon variable
_UPDATES=$(mktemp /tmp/updateslist.XXXXXX)   # Updates log variable
_DATE=$(date +"%A-%d-%B-%Y-%T")              # Date variable
_TMPU="/tmp/_updusr"
_PLAIN="/tmp/updateslist.txt"
_LLUPDATES="/var/log/llupdates.log"
if [ ! -f "$_TMPU" ]; then echo "$_MYUPDUSER" > "$_TMPU"; chmod 600 "$_TMPU"; fi
_SVUSER=$(cat "$_TMPU")

# function remove temp files
rm_temp_files() {
rm -f $_SVUSER
rm -f $_TMPU
rm -f $_UPDATES
rm -f $_PLAIN
rm -f /tmp/updateslist.*
}

# function view, save update log
view_save_log() {
  _ANS=$(yad --center --borders=15 --question --image=logviewer --text-info --width="780" --height="400" --window-icon="$_ICON" --title=" $_APPNAME Error log" \
                --button=gtk-cancel:1 --button="Save":0 --filename="$_LLUPDATES" 2>/dev/null); _OPT=$(echo $?)

    if [[ "$_ANS" =~ "Copy to clipboard" ]]; then
      sudo -u ${SUDO_USER:-$_SVUSER} xclip -selection c "$_LLUPDATES"
      xhost local:$_SVUSER > /dev/null 2>&1
      export DISPLAY=:0
      _NTFYICON="/usr/share/icons/Papirus/24x24/apps/liteupdates.png"
      notify-send -i "$_NTFYICON" 'Updates Log copied to clipboard' "Go to www.linuxliteos.com/forums and paste the log into a new or existing thread." -t 10000
      xhost - >/dev/null 2>&1; rm_temp_files; exit 0
    fi
    case $_OPT in
       0) szSavePath=$(yad --center --image=logviewer --borders=15 --title="    Save Updates Log" --width="600" --height="400" --window-icon="$_ICON" --file-selection --filename="/home/$_SVUSER/llupdates.log" \
                              --window-icon="$_ICON" --file-filter='*.log' --file-filter='All files | *' --save --confirm-overwrite 2>/dev/null)
           if [ "$?" -eq "0" ]; then 
		szSaveDir=$(dirname "$szSavePath")
		sudo cp "$_LLUPDATES" "$szSavePath"
		# Change ownership of the directory
                sudo chown $_SVUSER:$_SVUSER "$szSaveDir"
                sudo chown -R $_SVUSER:$_SVUSER "$szSavePath"
		rm_temp_files 
	   else rm_temp_files; exit 0 ; fi ;;
       1) rm_temp_files; exit 0 ;;
    esac
}

# Check Internet access
if eval "curl -sk google.com" >> /dev/null 2>&1; then :; else # Prompt ERROR internet connection check failed and exit
  yad --center --borders=15 --info --width="320" --height="120" --window-icon="$_ICON" --button=gtk-ok:1 --title="$_APPNAME" \
         --text="\n<span font='semibold 12' >Your computer is not connected to the Internet.</span> \n \nLinux Lite cannot check for Updates.\nPlease check your internet connection and try again.\n" --text-align center 2>/dev/null
  rm_temp_files; exit 0
fi

# Kill off any package managers that may be running
if [ "$(pidof synaptic)" ]; then killall -9 synaptic; fi
if [ ! -z "$(pgrep gdebi-gtk)" ]; then killall -9 gdebi-gtk; fi

# Get the full path to the script
SCRIPT_PATH="$(realpath "$0")"

# start dialog - Ask for elevation, else exit
if [ $EUID -ne 0 ]; then
  yad --center --borders=15 --image=system-software-install --question --width="400" --height="120" --window-icon="$_ICON" --button=gtk-cancel:1 --button="Continue":0 --title="$_APPNAME" \
       --text="<span font='Bold 12'>Linux Lite will now fetch the Updates list.</span>  \n\nClick <b>Cancel</b> to exit or <b>Continue</b> to proceed."  2>/dev/null
   case $? in
      0) pkexec "$SCRIPT_PATH"; if [ "${PIPESTATUS[@]}" -eq "126" ]; then rm_temp_files; fi; exit 0 ;;
      1) rm_temp_files; exit 0 ;;
   esac
fi

if [ "$?" -eq "0" ];then
  # xclip check/ install
  if [ -z  "$(dpkg -l | grep -E '^ii' | grep xclip)" ]; then
    apt-get install xclip -y | yad --center --borders=15 --image=system-software-install --progress --pulsate --window-icon="$_ICON" --auto-close --no-cancel --width="320" --height="80" --title="$_APPNAME" \
                                       --text="Preparing... please wait...\n" --text-align center --no-buttons --skip-taskbar 2>/dev/null
  fi

# Repositories registered
_SLIST=$(grep '^deb ' -c /etc/apt/sources.list)
_SLISTD=$(grep -R --exclude="*.save" '^deb ' -c /etc/apt/sources.list.d/ | grep -c /)
APTUPDATE=$(($_SLIST + $_SLISTD)) # Total of repositories registered, this is approximated
apt-get update 2>&1 | tee "$_LLUPDATES" |
awk -v total=$APTUPDATE '/^Ign|^ Get/{count++;$1=""} FNR { if (total != 0){percentage=int (1000*count/total);print (percentage > 1000),"\n#",substr($0, 0, 128) }; fflush(stdout)}' \
| yad --undecorated  --center --borders=35 --width=320  --progress --text="Updating package lists...\n" --text-align center --window-icon="$_ICON" --title="Updating Software Sources - please wait..." --no-buttons --skip-taskbar --auto-close 2>/dev/null
    if [ "${PIPESTATUS[0]}" -ne "0" ]; then
        unset APTUPDATE; rm_temp_files; unset _UPDATES
        sed -i '1 i\===========================\
Install Updates Error Log\
===========================\
\
Install Updates could not fetch the package cache information lists.\
Go to https://www.linuxliteos.com/forums/ and paste the log below into\na new or existing thread for assistance.\n\
============ Log ===========\n' "$_LLUPDATES"
        sleep 4 | yad --center --borders=15 --image=error --progress --pulsate --window-icon="error" --auto-close --no-cancel --width="380" --height="80" --title="$_APPNAME - Error" \
                         --text="Errors occurred while fetching packages cache information lists.\n\n✔ Retrieving error log, please wait...\n" --no-buttons --skip-taskbar --text-align center 2>/dev/null
        view_save_log; exit 1
    fi
  unset APTUPDATE

  # Creates a list in /tmp/updateslist
  apt-get --just-print dist-upgrade 2>&1 | perl -ne 'if (/Inst\s([\w,\-,\d,\.,~,:,\+]+)\s\[([\w,\-,\d,\.,~,:,\+]+)\]\s\(([\w,\-,\d,\.,~,:,\+]+)\)? /i) {print "Name: $1 INSTALLED: $2 AVAILABLE: $3\n"}' |
  awk '{print NR,":\t"$0}' > $_PLAIN
  sed '
  s:$::
  ' $_PLAIN >> $_UPDATES


  # Check for available updates; if none then remove /tmp/updateslist.XXXXXX and display up-to-date dialog
      if [  -z "$(cat $_PLAIN)"  ]; then
        rm_temp_files; unset _UPDATES
        yad --center --borders=15 --image=dialog-information --info --window-icon="$_ICON"  --width="320" --height="60" --title="  $_APPNAME" \
               --text="\nYour system is up to date.\n"  --text-align center 2>/dev/null \
		--button="Cancel":1 --button="Ok":0
        exit 0
      fi

# Erase existing available info
dpkg --clear-avail
else
  rm_temp_files; unset _UPDATES; exit 0
fi

# Call the yad dialog to show update list
yad --center --borders=15 --image=system-software-install --text-info  --window-icon="$_ICON" --button=gtk-cancel:1 --button="Update Now":0 --title="Available Updates" --width="760" --height="400" --fontname="Droid regular 10" --filename="$_UPDATES" 2>/dev/null
      if [ "$?" -eq "0" ];then
        # Continue script if no halt, remove tmp file and unset variables
        rm $_UPDATES; unset _UPDATES

#Begin upgrade
DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" dist-upgrade -y  2>&1 | tee "$_LLUPDATES" |
awk ' BEGIN { FS=" "; total=1;end_download=0} /upgraded/ {total= $1 + $3;FS="[ :]" } /^Get:[[:digit:]]+/ {printf "#Downloading %s %s %s\n",$7,$(NF-1),$NF;print int(($2 - 1) * 100 / total);
fflush(stdout)} /^\(Reading / {if (end_download==0){print 100;fflush(stdout);end_download=1}} /^(Preparing|Unpacking|Selecting|Processing|Setting|Download)/ {print "#", substr($0, 0, 128); fflush(stdout)}' \
  | ( yad --center --undecorated --borders=35 --width=320 --progress --window-icon="$_ICON" --percentage="0" --auto-close --text="Downloading package(s)...\nThis may take a while.\n" --text-align center --no-buttons --skip-taskbar  2>/dev/null;
      yad --center --undecorated --borders=35 --width=320 --progress --window-icon="$_ICON" --percentage="0" --auto-close --text="Installing and configuring package(s)...\nThis may take a while.\n" --text-align center --no-buttons --skip-taskbar 2>/dev/null)

    if [ "${PIPESTATUS[0]}" -ne "0" ]; then
      sed -i '1 i\===========================\
Install Updates Error log\
===========================\
Install Updates could not successfully download and install available updates.\
Go to https://www.linuxliteos.com/forums/ and paste the log below into a new or existing thread for assistance.\n\
============ Log ===========\n' "$_LLUPDATES"
      sleep 4 | yad --center --borders=15 --image=error --progress --pulsate --window-icon="error" --auto-close --no-cancel --width="320" --height="80" --title="$_APPNAME - Error" \
                       --text="<span font='semibold 12'>Errors occurred while updating.</span> \n\n✔ Retrieving errors log. Please wait...\n" --no-buttons --skip-taskbar 2>/dev/null
      view_save_log; exit 0
    fi

    # Halt updates script if user selects Cancel
    else
       rm_temp_files; unset _UPDATES
       yad --center --borders=15 --image=info --info --width="320" --button=gtk-ok:1 --timeout="6" --window-icon="$_ICON" --title="   $_APPNAME" --text="\n<span font='semibold 10' > Updates have been canceled.</span>\n" --text-align center 2>/dev/null
       exit 0
    fi

# If Ubuntu base package has been upgraded during Install Updates, /usr/share/plymouth/themes/ubuntu-text/ubuntu-text.plymouth
llverprnt=$(awk '{print}' /etc/llver)
checkdefplym="/usr/share/plymouth/themes/ubuntu-text/ubuntu-text.plymouth"

if grep -q 0x988592 "$checkdefplym"; then
  sed -i "s/^title=Ubuntu.*$/title=$llverprnt/g" /usr/share/plymouth/themes/ubuntu-text/ubuntu-text.plymouth ;
  sed -i "s/black=0x2c001e/black=0x000000/g" /usr/share/plymouth/themes/ubuntu-text/ubuntu-text.plymouth ;
  sed -i "s/white=0xffffff/white=0xffffff/g" /usr/share/plymouth/themes/ubuntu-text/ubuntu-text.plymouth ;
  sed -i "s/brown=0xff4012/brown=0xffff00/g" /usr/share/plymouth/themes/ubuntu-text/ubuntu-text.plymouth ;
  sed -i "s/blue=0x988592/blue=0x000000/g" /usr/share/plymouth/themes/ubuntu-text/ubuntu-text.plymouth && update-initramfs -u |
  yad --center --undecorated --borders=35 --progress --auto-close --pulsate --text="Updating boot configuration please wait...\n" --text-align center --no-buttons --skip-taskbar
else
	echo " "
fi

    PROCEED=$(yad --center --borders=15 --question --image=logviewer --width="440" --height="80" --window-icon="$_ICON" --title="           Linux Lite Updates" --text-align center --button="No":1 --button="Yes":0 --text="\nLinux Lite Updates completed successfully.\n\nWould you like to view the Linux Lite Updates log?" 2>/dev/null ; echo $?)
    if [ ${PROCEED} -eq 1 ]; then rm_temp_files; :; else
      yad --center --borders=15 --question --image=logviewer --text-info --width="750" --height="400" --window-icon="$_ICON" --button="Close":1 --button="Save":0 --title="  $_APPNAME Log" --filename="$_LLUPDATES" 2>/dev/null
        if [ $? -eq 0 ]; then
          # Save report
          szSavePath=$(yad --center --image=logviewer --borders=15 --width="600" --height="400" --window-icon="$_ICON" --title=" Save Updates Log" --file-selection --filename=/home/$_SVUSER/llupdates-"${_DATE}".txt --file-filter='*.txt' \
                              --file-filter='All files | *' --save --confirm-overwrite 2>/dev/null)
          szSaveDir=$(dirname "$szSavePath"); sudo cp "$_LLUPDATES" "$szSavePath"; sudo chown $_SVUSER:$_SVUSER "$szSaveDir" ; sudo chown -R $_SVUSER:$_SVUSER "$szSavePath";  rm_temp_files; else rm_temp_files; :
        fi
    fi
    if [ -s /var/run/reboot-required ]; then
        yad --center --question --image=gtk-convert --borders=15 --width="320" --height="100" --window-icon="$_ICON" --button="Continue using my computer:"1 --button="Restart Now":0 --title="  Linux Lite Updates" --text-align center \
             --text="<span font='semibold 12'>These updates require the system to be restarted for the changes to take effect.</span>\n\n <span font='11'>Would you like to restart now?</span>\n" 2>/dev/null    
        if [ "$?" -eq "0" ]; then reboot; else exit 0 ; fi
    fi

exit 0
