Lite Software
================

A gui tool to easily install and remove popular software.

![](https://imgur.com/5lFNal4.png)

![](https://imgur.com/MEpQlYA.png)

## License ![License](https://img.shields.io/badge/license-GPLv2-green.svg)

This project is under the GPLv2 license. Unless otherwise stated in individual files.

## This package also includes the following features:
- Install Updates
- Lite Network Shares
- Lite System Report
- Menu, Internet, Support link

## Install Updates:

![](https://imgur.com/aIlO2WR.png)


## Lite Network Shares:

![](https://imgur.com/oOKZX6h.png)

![](https://imgur.com/9z67lXI.png)

## Lite System Report:

![](https://imgur.com/ZnJd5S8.png)

![](https://imgur.com/3RuLGG3.png)

![](https://imgur.com/CBGtHNA.png)

![](https://imgur.com/AMgGdWC.png)

## HiDPI Settings (Settings Manager):

![](https://imgur.com/45KQ2eX.png)

## Firewall Config (Settings Manager):

![](https://imgur.com/w1V45xu.png)

## Authors
- [Jerry Bezencon](https://github.com/linuxlite/)
- [Ralphy](https://github.com/ralphys)
- [Misko-2083](https://github.com/Misko-2083/)
- [Johnathan "ShaggyTwoDope" Jenkins](https://github.com/shaggytwodope/)
- [gerito1](https://github.com/gerito1/) 
